class Bubble extends Phaser.Physics.Arcade.Sprite {
  constructor(scene, x, y) {
    super(scene, x, y, "bubble");
    this.scene = scene;
    scene.physics.world.enable(this);
    scene.add.existing(this);
    this.setBounce(0.5);
    this.setCollideWorldBounds(true);
    this.setGravityY(10);
  }
}
